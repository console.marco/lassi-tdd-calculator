class ArithmeticExpression
	@expr

	def initialize(expr)
		@expr = expr
	end


	def calculate()
		i = 0
		@expr.split(",").each do |v|
			i += v.to_i
		end
		i
	end

	def validate()
		return not(@expr.match(/\A([0-9]+,)*[0-9]*\Z/).nil?)
	end

end
