# lib/string_calculator.rb
class StringCalculator
	
	@expr
	@value
	@valid

	@validated
	@calculated
		

	@@EXPR_NUM = /\([0-9]+\)/
	@@EXPR_OPS = /[\+|\-|\*|\\]/
	@@EXPR_BINOPS = /[\-|\\]/
	@@EXPR_OPERANDS = /\([\+|\-|\*|\\|\(|\)|0-9|,]*\)/

	def initialize(expr)
		@expr = expr
		@validated = false
		@valid = false
		@calculated = false
		@value = 0
	end


	def self.split_level(in_str) 
		str = in_str.clone
		i = 0
		par = 0
		while i< str.length do
		  if str[i] == ")"
		    par += 1
		  end
		  if str[i] == "("
		    par -= 1
		  end
		  if str[i] == "," and par == 0
		    str[i] = "_"
		  end
		  i+=1
		end
		return str.split("_")
	end

	def self.validate_string(expr) 
		e = expr.strip
		if e =~ /\A#{@@EXPR_NUM}/
			return true
		end
		if e =~ /\A#{@@EXPR_OPS}#{@@EXPR_OPERANDS}/
			is_bin = false
			
			#Check whether this is a binary operator
			if e =~ /\A#{@@EXPR_BINOPS}/
				is_bin = true
			end

			e = e[2, e.length-3]
			e_split = split_level(e)

			if is_bin and e_split.length != 2
				return false
			end

			for e1 in e_split do 
				if StringCalculator.validate_string(e1) == false 
					return false
				end
			end

			return true
		end
		return false
	end


	def validate()
		if @validated == true
			return @valid
		end
		@valid = StringCalculator.validate_string(@expr)
		@validated = true
		return @valid
	end


	def self.calculate_string(str)
		e = str.strip
		#Atom
		if e =~ /\A#{@@EXPR_NUM}/
			ret = e[1, e.length-2]
			return ret.to_f
		end
		#Addition
		if e =~ /\A\+#{@@EXPR_OPERANDS}/
			ret = e[2, e.length-2]
			return StringCalculator.addition(ret)
		end
		#Subtraction
		if e =~ /\A\-#{@@EXPR_OPERANDS}/
			ret = e[2, e.length-2]
			return StringCalculator.subtraction(ret)
		end
		#Multiplication
		if e =~ /\A\*#{@@EXPR_OPERANDS}/
			ret = e[2, e.length-2]
			return StringCalculator.multiplication(ret)
		end
		#Division
		if e =~ /\A\\#{@@EXPR_OPERANDS}/
			ret = e[2, e.length-2]
			return StringCalculator.division(ret)
		end
	end

	def self.addition(str)
		ex_list = StringCalculator.split_level(str)
		ret = 0.0
		for ex in ex_list do
			ret += StringCalculator.calculate_string(ex)
		end

		return ret
	end

	def self.multiplication(str)
		ex_list = StringCalculator.split_level(str)
		ret = 1.0
		for ex in ex_list do
			ret *= StringCalculator.calculate_string(ex)
		end

		return ret	
	end

	def self.division(str)
		ex_list = StringCalculator.split_level(str)
		return StringCalculator.calculate_string(ex_list[0]) / StringCalculator.calculate_string(ex_list[1])
	end

	def self.subtraction(str)
		ex_list = StringCalculator.split_level(str)
		return StringCalculator.calculate_string(ex_list[0]) - StringCalculator.calculate_string(ex_list[1])
	end


	def calculate()
		if @validated == false
			validate()
		end
		if @valid == false
			raise 'Invalid Expression'
		end
		if @calculated == true
			return @value
		end 
		@value = StringCalculator.calculate_string(@expr)
		@calculated = true
		return @value
	end
end
