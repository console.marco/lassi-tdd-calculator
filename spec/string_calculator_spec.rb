# spec/string_calculator_spec.rb

require "string_calculator"

describe StringCalculator do
	before  do
		@obj = StringCalculator.new("")
	end

	describe ".validate_string" do
		context "given (3)" do
			it "returns true" do
				expect(StringCalculator.validate_string "(3)").to eql true
			end
		end
		context "given (1,2)" do
			it "returns false" do
				expect(StringCalculator.validate_string "(1, 2)").to eql false
			end
		end
		context "given +((3), (4))" do
			it "returns true" do
				expect(StringCalculator.validate_string "+((3), (4))").to eql true
			end
		end
		context "given -((3), (4))" do
			it "returns true" do
				expect(StringCalculator.validate_string "-((3), (4))").to eql true
			end
		end
		context "given -((3), (4), (2)))" do
			it "returns false" do
				expect(StringCalculator.validate_string "-((3), (4), (2)))").to eql false
			end
		end
		context "given *((3), (4))" do
			it "returns true" do
				expect(StringCalculator.validate_string "*((3), (4))").to eql true
			end
		end
		context "given \\((3), (4))" do
			it "returns true" do
				expect(StringCalculator.validate_string "\\((3), (4))").to eql true
			end
		end
		context "given \\((3), (4), (2)))" do
			it "returns false" do
				expect(StringCalculator.validate_string "\\((3), (4), (2)))").to eql false
			end
		end
		context "given +(-((3), (2)), (1) )" do
			it "returns true" do
				expect(StringCalculator.validate_string "+(-((3), (2)), (1) )").to eql true
			end
		end

	end
	describe ".split_level" do
		context "given +((3), (2)) , (1)" do
			it "returns an array of 2 elements" do
				expect(StringCalculator.split_level("+((3), (2)) , (1)").length()).to eql 2
			end
		end
	end
	describe ".validate" do
		it "validates a string only once" do
			allow(StringCalculator).to receive(:validate_string).and_return(true)

			stc = StringCalculator.new("(1)")

			stc.validate()
			stc.validate()

			expect(StringCalculator).to have_received(:validate_string).once
		end
	end

	describe ".calculate" do
		it "evaluates an expression only once" do
			allow(StringCalculator).to receive(:calculate_string).and_return(0)
		
			stc = StringCalculator.new("(1)")
			stc.instance_variable_set(:@validated, true)
			stc.instance_variable_set(:@valid, true)

			stc.calculate()
			stc.calculate()

			expect(StringCalculator).to have_received(:calculate_string).once
		end
		it "raises an exception if the expression is not valid" do
			allow(StringCalculator).to receive(:calculate_string).and_return(0)
		
			stc = StringCalculator.new("(1)")
			stc.instance_variable_set(:@validated, true)
			stc.instance_variable_set(:@valid, false)
			
			expect {stc.calculate}.to raise_error("Invalid Expression")
		end
	end

	describe ".calculate_string" do
		context "Given (2)" do
			it "returns 2" do
				expect(StringCalculator.calculate_string("(2)")).to eql 2.0 
			end
		end
		context "Given +((3), (2), (1))" do
			it "returns 6" do
				expect(StringCalculator.calculate_string("+((3), (2), (1))")).to eql 6.0 
			end
		end
		context "Given *((3), (3), (1))" do
			it "returns 9" do
				expect(StringCalculator.calculate_string("*((3), (3), (1))")).to eql 9.0 
			end
		end		
		context "Given -((2), (1))" do
			it "returns 3" do
				expect(StringCalculator.calculate_string("-((2), (1))")).to eql 1.0 
			end
		end
		context "Given \\((4), (2))" do
			it "returns 3" do
				expect(StringCalculator.calculate_string("\\((2), (1))")).to eql 2.0 
			end
		end		
		context "Given \\(+(*((4), (2)), -((2), (1))), (3))" do
			it "returns 3" do
				expect(StringCalculator.calculate_string("\\(+(*((4), (2)), -((2), (1))), (3))")).to eql 3.0 
			end
		end
	end

	context "With expression \\(+(*((4), (2)), -((2), (1))), (3))" do		
		before (:each) do
			@obj = StringCalculator.new("\\(+(*((4), (2)), -((2), (1))), (3))")
		end
		it "is a valid expression" do
			expect(@obj.validate).to eql true
		end

		it "has value equal 3.0" do
			expect(@obj.calculate).to eql 3.0
		end
	end

	context "With expression \\(+(*((4), (2)), &&((2), (1))), (3))" do		
		before (:each) do
			@obj = StringCalculator.new("\\(+(*((4), (2)), &&((2), (1))), (3))")
		end
		it "is NOT a valid expression" do
			expect(@obj.validate).to eql false
		end

		it "raises an exception when calculated" do
			expect {@obj.calculate}.to raise_error("Invalid Expression")
		end
	end

end


