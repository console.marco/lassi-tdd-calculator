require 'arexpr.rb'
require 'rails_helper.rb'


describe ArithmeticExpression do

fixtures :myexprs

	context "Given an empty string" do
		it "returns 0" do
			ae = ArithmeticExpression.new("")
			expect(ae.calculate).to eql 0
		end
	end
	context "Given 1, 2" do
		it "returns 3" do
			ae = ArithmeticExpression.new("1,2")
			expect(ae.calculate).to eql 3
		end
	end

context "Given and empty string; Validate " do
	it "returns false" do
		ae = ArithmeticExpression.new("")
		expect(ae.validate).to eql true
	end
end

context "Given a; Validate " do
	it "returns false" do
		ae = ArithmeticExpression.new("a")
		expect(ae.validate).to eql false
	end
end


context "Given 1,2,3; Validate " do
	it "returns false" do
		ae = ArithmeticExpression.new("1,2,3")
		expect(ae.validate).to eql true
	end
end

context "Given fixture_one; Validate " do
	it "returns true" do
		me = myexprs(:one)
		ae = ArithmeticExpression.new(me.expr_str)
		expect(ae.validate).to eql true
	end
end


context "Given fixture_one; Calculate " do
	it "returns 6" do
		me = myexprs(:one)
		ae = ArithmeticExpression.new(me.expr_str)
		expect(ae.calculate).to eql 6
	end
end


context "Given fixture_two; Validate " do
	it "returns false" do
		me = myexprs(:two)
		ae = ArithmeticExpression.new(me.expr_str)
		expect(ae.validate).to eql false
	end
end

end