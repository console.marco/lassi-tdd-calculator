class CreateMyexprs < ActiveRecord::Migration[6.1]
  def change
    create_table :myexprs do |t|
      t.string :expr_str

      t.timestamps
    end
  end
end
